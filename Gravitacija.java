import java.util.Scanner;

public class Gravitacija {
    public static void main (String[] args) {
       Scanner sc = new Scanner(System.in);
       
       System.out.println("Vnesite zeljeno visino:");
       double visina = sc.nextDouble();
       
       double pospesek = izracun(visina);
       izpis(visina,pospesek);
    }

    public static double izracun(double visina){
        double a = (6.674 * 5.972 * Math.pow(10, 13)) / Math.pow((6.371 * Math.pow(10, 6) + visina), 2);
        return a;        
    }

    public static void izpis (double visina, double gravitacija) {
	    System.out.println("Pospesek na nadmorski visini: " + visina + " je: " + gravitacija);
      }

}